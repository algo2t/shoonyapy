
from shoonyapy import ShoonyaApi, DataApi
import logging

import config

from datetime import datetime as dt
from time import sleep
import pandas as pd


logging.basicConfig(level=logging.DEBUG)


begin = dt.now()
print(begin)

data = DataApi()

shoonya = ShoonyaApi(config.username, config.password,
                     config.pan_or_dob, debug=True)

sbin = data.get_instrument_by_exchange_symbol('NSE', 'SBIN')

order_no = shoonya.place_order(sbin, 1, order_type=shoonya.ORDER_TYPE_LIMIT, price=472,
                               product=shoonya.PRODUCT_INTRADAY, txn_type=shoonya.TRANSACTION_TYPE_SELL, is_amo=True)

df = shoonya.orders()
df = df.loc[df['status'].str.contains('Pending'), [
    'order_no', 'status', 'segment', 'product']]
print(df)


end = dt.now()

duration = end - begin
print(end)
print(f"milliseconds taken :: {duration.microseconds/1000}")
