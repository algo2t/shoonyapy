from shoonyapy import ShoonyaApi, DataApi
import logging

import config

from datetime import datetime as dt
from time import sleep
import pandas as pd


# logging.basicConfig(level=logging.DEBUG)


begin = dt.now()
print(begin)

data = DataApi()

shoonya = ShoonyaApi(config.username, config.password,
                     config.pan_or_dob, debug=True)

shoonya.market_status(exchange='MCX')
shoonya.market_status(mkt_type=shoonya.MARKET_TYPE_OL)
shoonya.market_status(mkt_type='')

inst = data.get_instrument_by_exchange_symbol('NSE', 'SBIN')
inst = data.get_instrument_by_exchange_symbol('MCX', 'GOLDPETAL21AUGFUT')
print(data.ltp(inst).ltp)
df = data.history(inst)
print(df)
shoonya.positions()

scrip = data.get_instrument_by_token_or_security_id(229420)
print(scrip)


end = dt.now()

duration = end - begin
print(end)
print(f'milliseconds taken :: {duration.microseconds/1000}')
